import sys

from tango import DeviceProxy

from taurus import Manager, Factory
from taurus.qt.qtcore.util.emitter import DelayedSubscriber
from taurus.qt.qtgui.application import TaurusApplication
from taurus.qt.qtgui.panel import TaurusForm


app = TaurusApplication(sys.argv, cmd_line_parser=None,)
panel = TaurusForm()
attr_list = DeviceProxy('sys/tg_test/1').get_attribute_list()
model = ['sys/tg_test/1/{}'.format(attr) for attr in attr_list]
panel.setModel(model)

print('Disabling events '+'<'*40)
Manager().changeDefaultPollingPeriod(9000)
Factory('tango').set_tango_subscribe_enabled(0)

panel.show()

print('Reenabling events '+'<'*40)
subscriber = DelayedSubscriber('tango',parent=app,
                                sleep=10e3,pause=5)

sys.exit(app.exec_())
