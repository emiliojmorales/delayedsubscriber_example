import sys

from tango import DeviceProxy

from taurus import Manager, Factory
from taurus.qt.qtcore.util.emitter import DelayedSubscriber
from taurus.external.qt import Qt
from taurus.qt.qtgui.application import TaurusApplication
from taurus.qt.qtgui.display import TaurusLabel


app = TaurusApplication(sys.argv, cmd_line_parser=None,)
panel = Qt.QWidget()
layout = Qt.QHBoxLayout()
panel.setLayout(layout)

attr_list = DeviceProxy('sys/tg_test/1').get_attribute_list()

for attr in attr_list[:30]:
    w = TaurusLabel()
    layout.addWidget(w)
    w.model = "sys/tg_test/1/{}".format(attr)


print('Disabling events '+'<'*40)
Manager().changeDefaultPollingPeriod(9000)
Factory('tango').set_tango_subscribe_enabled(0)

panel.show()

print('Reenabling events '+'<'*40)
subscriber = DelayedSubscriber('tango',parent=app,
                                sleep=10e3,pause=5)


sys.exit(app.exec_())